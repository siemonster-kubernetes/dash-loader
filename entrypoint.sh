#!/bin/bash

ELASTICSEARCH_HOST=$(echo "$ELASTICSEARCH_HOST" |sed 's/https\?:\/\///')
ELASTIC_URL='https://'"${ELASTIC_USR}"':'"${ELASTIC_PWD}"'@'"${ELASTIC_HOST}"''
KIBANA="http://demo-kibana:5601"
TIME_FIELD="@timestamp"
DEFAULT_INDEX="wazuh-alerts-3.x-*"
DIR=/opt/siemonster/dashboards
DIR_DATA=/opt/siemonster/data
KIBANA_HEADER_CONTENT="Content-Type: application/json"
KIBANA_HEADER_XSRF="kbn-xsrf: anything"
KIBANA_HEADER_JWT="Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOjEyMzQ1LCJzdWIiOiJkZXZlbG9wQHNpZW1vbnN0ZXIuY29tIiwibmFtZSI6ImFkbWluIiwicm9sZXMiOiJhZG1pbiJ9.U3HoX5w2P2zcvL65fLBr-wNG4Bnm-me8YbC2em8WeoY"

# waiting for Elastic readiness
until [ "$(curl -k -s "$ELASTIC_URL/_cluster/health" | grep "green")" ]; do
    >&2 echo "Waiting for ES-cluster health to be green..."
    sleep 5
done
>&2 echo "Elastic is up"

# waiting for Kibana readiness
until curl -s $KIBANA:5601 -o /dev/null; do
    echo "Waiting for Kibana..."
    sleep 1
done
>&2 echo "Kibana is up"

# Set Index types
declare -a index_patterns=(
    "osint-*"
    "logs-endpoint-*"
    "logs-endpoint-syslog-*"
    "logs-endpoint-suricata-*"
    "logs-endpoint-bro-*"
    "logs-endpoint-firepower-*"
    "logs-endpoint-netscaler-*"
    "logs-endpoint-apache-*"
    "mitre-attack-*"
    "logs-endpoint-winevent-sysmon-*"
    "logs-endpoint-winevent-security-*"
    "logs-endpoint-winevent-system-*"
    "logs-endpoint-winevent-application-*"
    "logs-endpoint-winevent-wmiactivity-*"
    "logs-endpoint-winevent-powershell-*"
    "syslog-*"
)
sleep 2
for index in ${!index_patterns[@]}; do
    curl -f -XPOST \
        -H "$KIBANA_HEADER_CONTENT" \
        -H "$KIBANA_HEADER_XSRF" \
        -H "$KIBANA_HEADER_JWT" \
        "$KIBANA/api/saved_objects/index-pattern/${index_patterns[${index}]}" \
        -d"{\"attributes\":{\"title\":\"${index_patterns[${index}]}\",\"timeFieldName\":\"$TIME_FIELD\"}}"
    echo
done

# Credits to Cyb3rWard0g
# *********** Making Wazuh the default index ***************
curl -XPOST \
    -H "$KIBANA_HEADER_CONTENT" \
    -H "$KIBANA_HEADER_XSRF" \
    -H "$KIBANA_HEADER_JWT" \
    "$KIBANA/api/kibana/settings/defaultIndex" \
    -d"{\"value\":\"$DEFAULT_INDEX\"}"

# *********** Loading dashboards ***************
for file in ${DIR}/*.json
do
    echo "Loading dashboard ${file}"
    curl -XPOST \
        -H "$KIBANA_HEADER_XSRF" \
        -H "$KIBANA_HEADER_CONTENT" \
        -H "$KIBANA_HEADER_JWT" \
        "$KIBANA/api/kibana/dashboards/import" \
        -d @${file} || exit 1
    echo
done

# *********** importing data ***************
for file in ${DIR_DATA}/*.gz
do
    echo "Importing data ${file}"

    # decompress data
    fileDecompressed=$(echo $file | sed -e 's/\.gz//g')
    gzip -cdk $file > $fileDecompressed

    # find out index name
    indexName=$(head -1 $fileDecompressed | jq -r '._index')

    # run import procedure with elasticdump
    eval 'NODE_TLS_REJECT_UNAUTHORIZED=0 elasticdump --quiet --output="$ELASTIC_URL/$indexName" --input="$fileDecompressed"'

    # removing uncompressed file
    rm $fileDecompressed
    echo
done
# *********** delete extra index patterns ***************
curl -XDELETE \
        -H "$KIBANA_HEADER_XSRF" \
        -H "$KIBANA_HEADER_CONTENT" \
        -H "$KIBANA_HEADER_JWT" \
        "$KIBANA/api/saved_objects/index-pattern/mitre-attack-*"
curl -XDELETE \
        -H "$KIBANA_HEADER_XSRF" \
        -H "$KIBANA_HEADER_CONTENT" \
        -H "$KIBANA_HEADER_JWT" \
        "$KIBANA/api/saved_objects/index-pattern/logs-network-*"
