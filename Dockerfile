FROM alpine:3.7

RUN apk add --no-cache bash su-exec curl jq && \
    apk add --update nodejs nodejs-npm && \
    npm install elasticdump -g

RUN mkdir -pv /opt/siemonster/dashboards

ADD dashboards/ /opt/siemonster/dashboards/

ADD templates /opt/siemonster/templates/

ADD data /opt/siemonster/data/

COPY entrypoint.sh /entrypoint.sh

RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
